# Changelog

All notable changes to this project will be documented in this file. See [standard-version](https://github.com/conventional-changelog/standard-version) for commit guidelines.

## 1.1.0 (2020-09-13)


### Features

* Probando version ([da76d0e](https://bitbucket.org/renato545/retometricanodejs/commit/da76d0e4c15acf28cf331547c4b81f62cdee1e43))


### Bug Fixes

* Agregando la funcion de auto incrementar la version en el archivo package.json ([a4d8b77](https://bitbucket.org/renato545/retometricanodejs/commit/a4d8b77e8480020a69745807c371de199655a0f1))
* Corrigiendo la libreria de commitizen ([7c2ca15](https://bitbucket.org/renato545/retometricanodejs/commit/7c2ca15f0db78461f8f7fa01962fd475b9295139))

### 1.0.3 (2020-09-13)


### Bug Fixes

* Agregando la funcion de auto incrementar la version en el archivo package.json ([a4d8b77](https://bitbucket.org/renato545/retometricanodejs/commit/a4d8b77e8480020a69745807c371de199655a0f1))

### 1.0.2 (2020-09-13)
