# Retobase-nodejs
Base para reto Node Js con Express

## Requisitos
- docker 
- docker-compose

## Comando para ejecutar

Escribir el siguiente comando en el terminal
```shell
docker-compose up --build
```
Para visualizar el sistema entrar a la siguiente direccion

```
http://localhost/retoibm/sumar/[numero1]/[numero2]
```
Por ejemplo:

http://localhost/retoibm/sumar/40/2